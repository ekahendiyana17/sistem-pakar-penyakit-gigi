<?php require_once('../Connections/koneksi.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
  function GetSQLValueString($koneksi, $theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
  {
    if (PHP_VERSION < 6) {
      $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
    }

    $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($koneksi, $theValue) : mysqli_escape_string($koneksi, $theValue);

    switch ($theType) {
      case "text":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "long":
      case "int":
        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
        break;
      case "double":
        $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
        break;
      case "date":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "defined":
        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
        break;
    }
    return $theValue;
  }
}

if ((isset($_GET['id_bobot'])) && ($_GET['id_bobot'] != "")) {
  $deleteSQL = sprintf(
    "DELETE FROM bobot WHERE id_bobot=%s",
    GetSQLValueString($koneksi,$_GET['id_bobot'], "int")
  );

  mysqli_select_db($database_koneksi, $koneksi);
  $Result1 = mysqli_query($deleteSQL, $koneksi) or die(mysqli_error($koneksi));

  $deleteGoTo = "bobot_read.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

mysqli_select_db($database_koneksi, $koneksi);
$query_rsBobotDelete = "SELECT * FROM bobot ORDER BY id_bobot ASC";
$rsBobotDelete = mysqli_query($query_rsBobotDelete, $koneksi) or die(mysqli_error($koneksi));
$row_rsBobotDelete = mysqli_fetch_assoc($rsBobotDelete);
$totalRows_rsBobotDelete = mysqli_num_rows($rsBobotDelete);
?>
<?php
mysqli_free_result($rsBobotDelete);
?>
