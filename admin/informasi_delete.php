<?php require_once('../Connections/koneksi.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($koneksi, $theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($koneksi, $theValue) : mysqli_escape_string($koneksi, $theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_GET['id_info'])) && ($_GET['id_info'] != "")) {
  $deleteSQL = sprintf("DELETE FROM informasi WHERE id_info=%s",
                       GetSQLValueString($koneksi,$_GET['id_info'], "text"));

  mysqli_select_db($database_koneksi, $koneksi);
  $Result1 = mysqli_query($deleteSQL, $koneksi) or die(mysqli_error($koneksi));
unlink("foto_informasi/".$_GET['id_info'].".jpg");
  $deleteGoTo = "informasi_read.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

mysqli_select_db($database_koneksi, $koneksi);
$query_rsInfoDelete = "SELECT * FROM informasi ORDER BY id_info ASC";
$rsInfoDelete = mysqli_query($query_rsInfoDelete, $koneksi) or die(mysqli_error($koneksi));
$row_rsInfoDelete = mysqli_fetch_assoc($rsInfoDelete);
$totalRows_rsInfoDelete = mysqli_num_rows($rsInfoDelete);
?>
<?php
mysqli_free_result($rsInfoDelete);
?>
