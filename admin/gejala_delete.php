<?php require_once('../Connections/koneksi.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($koneksi, $theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($koneksi, $theValue) : mysqli_escape_string($koneksi, $theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_GET['kd_gejala'])) && ($_GET['kd_gejala'] != "")) {
  $deleteSQL = sprintf("DELETE FROM gejala WHERE kd_gejala=%s",
                       GetSQLValueString($koneksi,$_GET['kd_gejala'], "text"));

  mysqli_select_db($koneksi, $database_koneksi);
  $Result1 = mysqli_query($koneksi, $deleteSQL) or die(mysqli_error($koneksi));

  $deleteGoTo = "gejala_read.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

mysqli_select_db($koneksi, $database_koneksi);
$query_rsHapusGejala = "SELECT * FROM gejala ORDER BY kd_gejala ASC";
$rsHapusGejala = mysqli_query($koneksi, $query_rsHapusGejala) or die(mysqli_error($koneksi));
$row_rsHapusGejala = mysqli_fetch_assoc($rsHapusGejala);
$totalRows_rsHapusGejala = mysqli_num_rows($rsHapusGejala);
?>
<?php
mysqli_free_result($rsHapusGejala);
?>
