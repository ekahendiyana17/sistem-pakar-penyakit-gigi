<?php require_once('../Connections/koneksi.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($koneksi, $theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($koneksi, $theValue) : mysqli_escape_string($koneksi, $theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_GET['id_indikator'])) && ($_GET['id_indikator'] != "")) {
  $deleteSQL = sprintf("DELETE FROM indikator WHERE id_indikator=%s",
                       GetSQLValueString($koneksi,$_GET['id_indikator'], "int"));

  mysqli_select_db($koneksi, $database_koneksi);
  $Result1 = mysqli_query($koneksi, $deleteSQL or die(mysqli_error($koneksi))

  $DeleteGoTo = "indikator_read.php");
  if (isset($_SERVER['QUERY_STRING'])) {
    $DeleteGoTo .= (strpos($DeleteGoTo, '?')) ? "&" : "?";
    $DeleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $DeleteGoTo));
}

mysqli_select_db($database_koneksi, $koneksi);
$query_rsIndikatorDelete = "SELECT * FROM indikator ORDER BY id_indikator ASC";
$rsIndikatorDelete = mysqli_query($koneksi, $query_rsIndikatorDelete) or die(mysqli_error($koneksi));
$row_rsIndikatorDelete = mysqli_fetch_assoc($rsIndikatorDelete);
$totalRows_rsIndikatorDelete = mysqli_num_rows($rsIndikatorDelete);
?>
<?php
mysqli_free_result($rsIndikatorDelete);
?>
